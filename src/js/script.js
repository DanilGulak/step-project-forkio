const burger = document.querySelector('.burger')

document.body.addEventListener('click', (e) => {
    const target = e.target;
    if (target.classList.contains('burger') || target.classList.contains('burger__line')) {
        burger.classList.toggle('burger--opened');
    } else if (!target.classList.contains('nav-menu__link') && !target.classList.contains('nav-menu__items')){
        burger.classList.remove('burger--opened');
    }
})



